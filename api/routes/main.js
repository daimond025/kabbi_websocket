'use strict';
const express = require('express');
const router = express.Router();
const logger = require('../../services/logger').chatLogger;
const serviceInfo = require('../../services/serviceInfo');
const lib = require("../../services/lib");
const config = lib.getConfig();


router.use(function (req, res, next) {
    logger('info', 'method called: ' + req.method + ' ' + req.url);
    logger('info', 'query params: ' + JSON.stringify(req.query));
    next();
});


/**
 * Main service handler
 */
router.get('/', (req, res) => {
    res.send('Hi! I am web-socket service');
});

/**
 * Service version
 */
router.get('/version', serviceInfo.version);

/**
 * Service status
 */
router.get('/status', serviceInfo.status);

/**
 * Get service active clients data
 */
router.get('/active_clients', (req, res) => {
    const key = req.query.key;
    if (key === config.MY_SECRET_KEY) {
        res.send(JSON.stringify(req.activeState.clients));
        return;
    }
    res.statusCode = 403;
    res.send(JSON.stringify('auth_error'));
});

/**
 * Get common count of active clients
 */
router.get('/count_clients', (req, res) => {
    const key = req.query.key;
    if (key === config.MY_SECRET_KEY) {
        res.send(JSON.stringify(req.activeState.activeSockets.length));
        return;
    }
    res.statusCode = 403;
    res.send(JSON.stringify('auth_error'));
});

/**
 * Get active rooms
 */
router.get('/active_rooms', (req, res) => {
    const key = req.query.key;
    if (key === config.MY_SECRET_KEY) {
        res.send(JSON.stringify(req.activeState.rooms));
        return;
    }
    res.statusCode = 403;
    res.send(JSON.stringify('auth_error'));
});

module.exports = router;