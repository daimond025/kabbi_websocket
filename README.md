## Common description  

Сервис представляет собой веб-сокет сервер, реализующий две основных функции:  
*  Чат   
    - Общий чат филиала  
    - Приватный чат между диспетчерами  
    - Чат между диспетчерами и водителем  
*  Сокет сервер для приложения исполнителей. Релизует real-time составляющую взаимодействия приложения с сервером.  

## Requirements version of Node.js
Node version : ^v.6.9.1

## Install Dependencies

```bash
npm install
```
    
## How to run server
* Заполнить .env файл по примеру .env.example файла


* Проверить корректность .env файла: 
```bash
npm run check-env
``` 
При успешном выполнении в консоль выведет: OK. В случае ошибки в консоль выведет описание ошибки.

* Запустить сервер: 
```bash
npm run start
```

@TODO
* Задать параметры памяти такие как –max-old-space-size  и тд.
## Chat API description
Описание протокола для приложения:  
После коннекта приложению посылается:  
send_client_data, на что он должен послать save_client_data
1.  Послать свои данные на сервер:  
    Command: save_client_data  
    Params: 
```bash
    {
                  'client_type': 'worker',
                  'client_id': callsign,
                  'client_role': 'водитель',
                  'client_f': 'Иванова',
                  'client_i': 'Иван',
                  'client_o': 'Петрович',
                  'city_arr': ['26068','568563'],
                  'tenant_login': '3colors',
                  'socket_id':  'qadsasdasdasd',
                  'device': 'ios'
    };
```
2.  Отправить сообщение:   
    Command: new_message  
    Params:
```bash
     {
                    'receiver_type': worker/main_city_chat,
                    'receiver_id': callsign/city_id,
                    'city_id': cityId,
                    'message': 'messageData',
                    'message_time': '29.07 13:25'
    }
```                
3.  Получить сообщение  
    Сommand: new_message  
    Result:    
```bash
   {
                    'city_id': cityId,
                    'receiver_id': receiverId,
                    'receiver_type': receiverType,
                    'sender_id': client.client_id,
                    'sender_type': client.client_type,
                    'sender_role': client.client_role,
                    'sender_f': client.client_f,
                    'sender_i': client.client_i,
                    'sender_o': client.client_o,
                    'time': messageTime,
                    'timestamp': timeStamp,
                    'message': message
     }
```
4. Отправить запрос на получение последних n сообщений  
   Сommand: get_last_messages  
   Params:
```bash
    {
                    'tenant_login': tenant_login,
                    'receiver_id': callsign,
                    'receiver_type': 'worker',
                    'city_id': 26068,
                    'count': 10
    }
```
5.  Отправить запрос на получение последних n сообщений  
    Сommand: get_last_messages  
    Result:  
```bash 
    [{
                    'city_id': cityId,
                    'receiver_id': receiverId,
                    'receiver_type': receiverType,
                    'sender_id': client.client_id,
                    'sender_type': client.client_type,
                    'sender_role': client.client_role,
                    'sender_f': client.client_f,
                    'sender_i': client.client_i,
                    'sender_o': client.client_o,
                    'time': messageTime,
                    'timestamp': timeStamp,
                    'message': message
    }]
```
6. Получить последние n сообщений  
   Command: last_messages  
   Result:  
```bash
```
7.  Отпрвить запрос на получение онлайн юзеров   
    Command: get_online_users  
    Params:  
```bash  
     {
                   "tenant_login": tenant_login,
                   "city_arr": ['26068']
     }
```
8.  Получение онлайн юзеров   
    Command: online_users  
    Result:  
```bash  
    {
    }
```
9.  Отправить запрос на получение  непрочитанных сообщений  
    Command: get_unreaded_message     
    Params:    
```bash
    {
                   'tenant_login': tenant_login,
                   'client_type': 'user',
                   'client_id': client_id
     } 
```
10.  Получить непрочитанные сообщения  
    Command: unreaded_message     
    Result:    
```bash
    {
          
    } 
```
11. Сделать сообщение прочитанным  
    Command: message_is_readed     
    Params:    
```bash
   {
                    'tenant_login': tenant_login,
                    'city_id': cityId,
                    'sender_id': null,
                    'sender_type': null,
                    'receiver_id': callsign,
                    'receiver_type': worker
    }
```
12. Отправить запрос на получение сообщений более ранних чем  last_timestamp  
    Command: get_history_messages     
    Params:    
```bash
    {
                    'receiver_type': 'worker',
                    'receiver_id': callsign,
                    'city_id': cityId,
                    'count': 3,
                    'last_timestamp': lastTimestamp,
                    'tenant_login': tenant_login
    }
```
13. Получение сообщений более ранних чем  last_timestamp  
    Command: last_messages     
    Result:    
```bash
```
14. Отправить запрос на получение сообщений более поздних чем  last_timestamp  
    Command: get_history_messages     
    Params:    
```bash
    {
                    'receiver_type': 'worker',
                    'receiver_id': callsign,
                    'city_id': cityId,
                    'count': 3,
                    'last_timestamp': lastTimestamp,
                    'tenant_login': tenant_login
    }
```
15. Получение сообщений более поздних чем  last_timestamp  
    Command: next_messages     
    Result:    
```bash
``` 