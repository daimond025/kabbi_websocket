'use strict';

exports.getConfig = () => {
    return require('dotenv-safe').load().parsed;
};

exports.getServiceVersion = () => {
    return require('../package.json').version;
};

exports.capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};