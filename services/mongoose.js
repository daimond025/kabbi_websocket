'use strict';
const mongoose = require('mongoose');
const fs = require("fs");
const lib = require("./lib");
const config = lib.getConfig();
const logger = require('./logger');
const chatLogger = logger.chatLogger;


const mongoDSN = config.MONGODB_MAIN_DSN;
const options = {
    server: {
        socketOptions: {
            autoReconnect: true,
            keepAlive: 300000,
            connectTimeoutMS: 30000
        },
        reconnectTries: Number.MAX_VALUE
    },
    replset: {
        socketOptions: {
            keepAlive: 300000,
            connectTimeoutMS: 30000
        }
    }
};
mongoose.Promise = global.Promise;
mongoose.connect(mongoDSN, options);
const db = mongoose.connection;

db.on('error', (err) => {
    chatLogger('error', 'connection error:', err.message);
});

db.on('disconnected', () => {
    chatLogger('error', 'mongoose default connection disconnected');
});

db.once('open', () => {
    chatLogger("info", "connected to MongoDB at " + mongoDSN);
});

const Schema = mongoose.Schema;

// Schemas

const Chat = new Schema({
    room_id: {type: String},
    is_private: {type: Boolean},
    is_readed: {type: Boolean},
    city_id: {type: Number},
    tenant_id: {type: Number},
    tenant_domain: {type: String},
    receiver_id: {type: Number},
    receiver_type: {type: String},
    sender_type: {type: String},
    sender_role: {type: String},
    sender_id: {type: Number},
    sender_f: {type: String},
    sender_i: {type: String},
    sender_o: {type: String},
    sender_time: {type: String},
    timestamp: {type: Number},
    message: {type: String},
    uuid: {type: String},
    modified: {type: Date, default: Date.now}
});

Chat.index({room_id: 1, timestamp: -1});

const ChatModel = mongoose.model('Chat', Chat);


const Check = new Schema({
    key: {type: String},
    value: {type: String},
}, {collection: 'check'});

const CheckModel = mongoose.model('check', Check);

/**
 * check insert
 * @param key
 * @param value
 * @return {Promise}
 */
function checkInsert(key, value) {
    return new Promise((resolve, reject) => {
        const check = new CheckModel({
            key: key,
            value: value,
        });
        check.save((err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

/**
 * check select
 * @param key
 * @return {Promise}
 */
function checkSelect(key) {
    return new Promise((resolve, reject) => {
        CheckModel.findOne({'key': key}, (err, row) => {
            if (err) {
                reject(err);
            } else {
                if (row) {
                    resolve(row);
                } else {
                    reject(new Error(`mongodb: can not select row for key: ${key}`))
                }
            }
        });
    });

}

/**
 * check update
 * @param key
 * @param value
 * @return {Promise}
 */
function checkUpdate(key, value) {
    return new Promise((resolve, reject) => {
        CheckModel.findOneAndUpdate({key: key}, {$set: {value: value}}, (err, row) => {
            if (err) {
                reject(err);
            } else {
                if (row) {
                    resolve(row);
                } else {
                    reject(new Error(`mongodb: can not update row for key: ${key} with value: ${value}`));
                }

            }
        });
    });

}
/**
 * check delete
 * @param key
 * @return {Promise}
 */
function checkDelete(key) {
    return new Promise((resolve, reject) => {
        CheckModel.findOneAndRemove({key: key}, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });

}

module.exports = {
    ChatModel,
    checkInsert,
    checkUpdate,
    checkDelete,
    checkSelect
};
