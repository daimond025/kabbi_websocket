const request = require('request');
const config = require("../lib").getConfig();
const apiServiceUrl = config.API_SERVICE_URL;

/**
 * Send email
 * @param {number} tenantId
 * @param {string} type
 * @param {string} to
 * @param {string} cityId
 * @param {string} lang
 * @param {object} emailParams
 * @return {Promise<number>}
 */
function sendEmail(tenantId, type, to, cityId, lang, emailParams) {
    return new Promise((resolve, reject) => {
        const getString = `tenant_id=${tenantId}&type=${type}&to=${to}&city_id=${cityId}`;

        Object.keys(emailParams).map((key, index) => {
            if (emailParams[key] === null) {
                emailParams[key] = '';
            }
        });

        const requestOptions = {
            url: `${apiServiceUrl}v1/email/send?${getString}`,
            formData: emailParams
        };

        function sendRequest(error, response, body) {
            if (error) {
                return reject(error);
            }
            if (parseInt(response.statusCode) !== 200) {
                return reject(new Error(`bad response status code:${response.statusCode}`));
            }

            return resolve(1);
        }

        request.post(requestOptions, sendRequest);
    })
}

exports.sendEmail = sendEmail;
