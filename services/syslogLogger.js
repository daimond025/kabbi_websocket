'use strict';
const syslog = require('modern-syslog');
const config = require('./lib').getConfig();
const serviceVersion = require('./lib').getServiceVersion();


/**
 * Chat Logger
 * @param  {String} category
 * @param  {String} message
 */
function chatLogger(category, message) {
    const prefix = '[main] ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_CHAT_IDENT + '-' + serviceVersion, null, config.SYSLOG_CHAT_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}


/**
 * Order logger
 * @param  {Number} orderId
 * @param  {String} category
 * @param  {String} message
 */
function orderLogger(orderId, category, message) {
    const prefix = '[order] order_id=' + orderId + ' ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_ORDER_IDENT + '-' + serviceVersion, null, config.SYSLOG_ORDER_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}


/**
 * Worker socket logger
 * @param  {String} workerQueueLabel
 * @param  {String} category
 * @param  {String} message
 */
function workerSocketLogger(workerQueueLabel, category, message) {
    const prefix = '[worker_queue] worker_queue_label=' + workerQueueLabel + ' ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message);
    }
    message = prefix + message;
    syslog.init(config.SYSLOG_WORKER_QUEUE_IDENT + '-' + serviceVersion, null, config.SYSLOG_WORKER_QUEUE_FACILITY);
    switch (category) {
        case 'info':
            syslog.log(syslog.level.LOG_INFO, message);
            break;
        case 'error':
            syslog.log(syslog.level.LOG_ERR, message);
            break;
        default:
            syslog.log(syslog.level.LOG_INFO, message);
    }
}

exports.chatLogger = chatLogger;
exports.orderLogger = orderLogger;
exports.workerSocketLogger = workerSocketLogger;

