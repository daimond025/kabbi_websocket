"use strict";

const lib = require("./lib");
const config = lib.getConfig();
const mysql = require('mysql');
const logger = require('./logger');
const chatLogger = logger.chatLogger;

/**
 * Sql Manager
 * @return {SqlManager}
 */
function SqlManager() {
    this.pool = mysql.createPool({
        host: config.DB_MAIN_HOST,
        user: config.DB_MAIN_USERNAME,
        password: config.DB_MAIN_PASSWORD,
        database: config.DB_MAIN_NAME,
        connectionLimit: parseInt(config.DB_MAIN_CONNECTION_LIMIT),
        connectTimeout: parseInt(config.DB_MAIN_CONNECTION_TIMEOUT),
        acquireTimeout: parseInt(config.DB_MAIN_ACQUIRE_TIMEOUT),
    });

    this.pool.on('connection', connection => {
        chatLogger("info", "mysql pool: connected!");
    });

    this.pool.on('enqueue', () => {
        chatLogger("info", "mysql pool: waiting for available connection slot");
    });

    this.pool.on('error', (err) => {
        chatLogger("error", `mysql pool: error:${err.message}`);
    });
}

/**
 * Get tenant id by domain
 * @param {String} tenantDomain
 * @param  {Function} callback
 */
SqlManager.prototype.getTenantIdByDomain = function (tenantDomain, callback) {
    const self = this;
    self.pool.getConnection((err, connection) => {
        if (err) {
            return callback(err);
        }
        const selectVals = [tenantDomain];
        connection.query('SELECT `tenant_id` FROM `tbl_tenant` where domain= ?', selectVals,
            (err, rows) => {
                if (err) {
                    connection.release();
                    return callback(err);
                }
                connection.release();
                if (rows.length === 0) {
                    return callback(null, null);
                }
                return callback(null, rows[0].tenant_id);
            });
    });
};

SqlManager.prototype.getNotifyUsersData = function (tenantId, cityId) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const selectValues = [tenantId, cityId];
            connection.query(`select u.email,u.name as first_name,u.last_name,u.second_name,u.lang,c.* from tbl_user u 
            join tbl_user_has_city uc on u.user_id=uc.user_id
            join tbl_city  c on uc.city_id = c.city_id
            where tenant_id= ? and active=1 and chat_email_notify=1 and uc.city_id= ? and email_confirm=1`, selectValues,
                (err, rows) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    }
                    connection.release();
                    if (rows.length === 0) {
                        return reject(new Error('No data'));
                    }
                    return resolve(rows);
                });
        });
    })
};


/**
 * Check insert to db
 * @param key
 * @param value
 * @return {Promise}
 */
SqlManager.prototype.checkInsert = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const insertVals = [key, value];
            connection.query("INSERT INTO `tbl_check` set `key` = ?, `value` = ?", insertVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve();
                    }
                });
        });
    });
};

/**
 * Check select from db
 * @param key
 * @return {Promise}
 */
SqlManager.prototype.checkSelect = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const selectVals = [key];
            connection.query("SELECT * from `tbl_check` where `key` = ?", selectVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve(result);
                    }
                });
        });
    });
};

/**
 * Check update to db
 * @param key
 * @param value
 * @return {Promise}
 */
SqlManager.prototype.checkUpdate = function (key, value) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const updateVals = [value, key];
            connection.query('UPDATE  `tbl_check` SET `value` = ? where `key` = ?', updateVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve();
                    }
                });
        });
    });
};

/**
 * Check delete from db
 * @param key
 * @return {Promise}
 */
SqlManager.prototype.checkDelete = function (key) {
    const self = this;
    return new Promise((resolve, reject) => {
        self.pool.getConnection((err, connection) => {
            if (err) {
                return reject(err);
            }
            const updateVals = [key];
            connection.query('DELETE from `tbl_check` where `key` = ?', updateVals,
                (err, result) => {
                    if (err) {
                        connection.release();
                        return reject(err);
                    } else {
                        connection.release();
                        return resolve();
                    }
                });
        });
    });
};


module.exports = exports = new SqlManager();
