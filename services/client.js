'use strict';

const _ = require('underscore');
const sqlManager = require('./sqlManager');

function Client(id, clientType, clientId, clientRole, clientF, clientI, clientO, device, device_info, tenantLogin, city_arr, resCallback) {
    this.id = id;
    this.client_type = clientType;
    this.client_id = clientId;
    this.client_role = clientRole;
    this.client_f = clientF;
    this.client_i = clientI;
    this.client_o = clientO;
    this.inroom = [];
    this.device = device;
    this.device_info = device_info;
    this.city_arr = city_arr;
    this.tenant_login = tenantLogin;
    this.tenant_id = null;
    this.setTenantId(this.tenant_login, (err, result) => {
        resCallback(err, true);
    })
}

Client.prototype.setTenantId = function (tenantDomain, resCallback) {
    const self = this;
    sqlManager.getTenantIdByDomain(tenantDomain, (err, tenantId) => {
        if (err) {
            return resCallback(err);
        } else {
            self.tenant_id = tenantId;
            return resCallback(null, tenantId);
        }
    })

};

Client.prototype.addRoom = function (roomID) {
    const self = this;
    self.inroom.push(roomID);
};

Client.prototype.removeRoom = function (roomID) {
    const self = this;
    if (_.contains((self.inroom), roomID)) {
        const roomIndex = self.inroom.indexOf(roomID);
        self.inroom.splice(roomIndex, 1);
    }
};

Client.prototype.isInRoom = function (roomID) {
    const self = this;
    return _.contains((self.inroom), roomID)
};

Client.prototype.isInCity = function (cityID) {
    const self = this;
    return _.contains((self.city_arr), cityID);
};


module.exports = Client;