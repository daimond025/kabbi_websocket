'use strict';

/**
 * Room
 * @param  id
 * Для города- tenantLogin_cityId;
 * Для исполнителя - tenantLogin_driverCallsign
 * Для юзера - tenantLogin_userId
 * @param  tenant_login
 * @param  city_id
 * @param  owner_type
 * @param  privateRoom
 * @returns {Room}
 */
function Room(id, tenant_login, city_id, owner_type, privateRoom) {
    this.id = id;
    this.clients = [];
    this.status = "available";
    this.tenant_login = tenant_login;
    this.city_id = city_id;
    this.owner_type = owner_type;
    this.private = privateRoom;
}


Room.prototype.addClient = function (clientID) {
    const self = this;
    if (self.status === "available") {
        self.clients.push(clientID);
    }
};


Room.prototype.removeClient = function (client) {
    const self = this;
    let clientIndex = -1;
    for (let i = 0; i < self.clients.length; i++) {
        if (self.clients[i].id === client.id) {
            clientIndex = i;
            break;
        }
    }
    self.clients.remove(clientIndex);
};


Room.prototype.isAvailable = function () {
    const self = this;
    return self.status === "available";
};

Room.prototype.isPrivate = function () {
    const self = this;
    return self.private;
};

module.exports = Room;