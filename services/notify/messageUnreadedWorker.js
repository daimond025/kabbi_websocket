'use strict';
const ChatModel = require('../mongoose').ChatModel;
const logger = require('../logger');
const chatLogger = logger.chatLogger;
const sqlManager = require('../sqlManager');
const lib = require("../lib");
const serviceApi = require('../externalWebServices/serviceApi');


module.exports = function (messageUuid) {
    const checkTimeout = 5;// 10 sec to check message is read
    chatLogger('info', `messageUnreadedWorker->Email worker for message: ${messageUuid} will be call after ${checkTimeout} seconds`);
    setTimeout(() => {
        ChatModel.findOne({
            'uuid': messageUuid,
            'is_readed': false
        }).exec((err, message) => {
            if (err) {
                chatLogger('error', `messageUnreadedWorker->chatModel.findOne error: ${err.message}`);
                return;
            }
            if (!message) {
                chatLogger('info', `messageUnreadedWorker->message: ${messageUuid} is read`);
                return;
            }
            chatLogger('info', `messageUnreadedWorker->message: ${messageUuid} is unread: ${message}`);
            runEmailNotify(message);
        });
    }, checkTimeout * 1000, messageUuid)
};

function runEmailNotify(message) {
    const tenantId = message.tenant_id;
    const cityId = message.city_id;
    const workerCallsign = message.sender_id;
    let workerLastName = message.sender_f;
    let workerName = message.sender_i;
    let workerPatronymicName = message.sender_o;
    if (typeof workerName === "string" && workerName.length > 0) {
        workerName = workerName[0].toUpperCase();
    }
    if (typeof workerPatronymicName === "string" && workerPatronymicName.length > 0) {
        workerPatronymicName = workerPatronymicName[0].toUpperCase();
    }
    const workerFio = `${workerLastName} ${workerName}. ${workerPatronymicName}.`;
    const sendTime = message.sender_time;
    const msgText = message.message;
    const senderRole = lib.capitalizeFirstLetter(message.sender_role);
    sqlManager.getNotifyUsersData(tenantId, cityId)
        .then(users => {
            if (Array.isArray(users)) {
                for (let user of users) {
                    const email = user.email.toString();
                    const clientName = user.first_name;
                    let lang = user.lang;
                    const langArr = lang.split('-');
                    lang = langArr[0];
                    let cityName = user.name;
                    let cityTranslatedColumnName = 'name_' + lang;
                    if (user[cityTranslatedColumnName]) {
                        cityName = user[cityTranslatedColumnName];
                    }
                    const emailParams = {
                        user_name: clientName,
                        sender_name: workerFio,
                        sender_role: senderRole,
                        sender_id: workerCallsign,
                        city: cityName,
                        send_time: sendTime,
                        message: msgText,
                    };
                    serviceApi.sendEmail(tenantId, 'chat_unread_message', email, cityId, lang, emailParams)
                        .then(result => {
                            chatLogger('info', `messageUnreadedWorker->serviceApi.sendEmail to tenant_id: ${tenantId}, email: ${email}. Result: ${result}`);
                        })
                        .catch(err => {
                            chatLogger('error', `messageUnreadedWorker->serviceApi.sendEmail to tenant_id: ${tenantId}, email: ${email}. Error: ${err.message}`);
                        });
                }
            }
        })
        .catch(err => {
            chatLogger('error', err.message);
        });
}