'use strict';

const packageInfo = require('../package.json');
const mongoCheckInsert = require('./mongoose').checkInsert;
const mongoCheckUpdate = require('./mongoose').checkUpdate;
const mongoCheckSelect = require('./mongoose').checkSelect;
const mongoCheckDelete = require('./mongoose').checkDelete;
const request = require('request');
const lib = require("./lib");
const config = lib.getConfig();
const push_Api = config.SERVICE_PUSH_URL;
const sqlManager = require('./sqlManager');
const uuid = require('uuid/v4');

function version(req, res, next) {
    const name = packageInfo.name;
    const version = packageInfo.version;
    res.setHeader('Content-Type', 'text/plain');
    let info = `${name} v${version}
work on nodejs ${process.version}`;
    res.send(info);
}

function status(req, res, next) {
    const STATUS_OK = 'OK   ';
    const STATUS_ERROR = 'ERROR';
    const name = packageInfo.name;
    const version = packageInfo.version;
    let isNot = ' ';
    let hasError = false;
    let dbStatus, dbError,
        mongoStatus, mongoError,
        pushApiStatus, pushApiError;
    dbChecker()
        .then(() => {
            dbStatus = STATUS_OK;
            dbError = '';
        })
        .catch(redisErr => {
            dbStatus = STATUS_ERROR;
            dbError = `(${redisErr.message})`;
            hasError = true;
        })
        .then(() => {
            return mongoChecker();
        })
        .then(() => {
            mongoStatus = STATUS_OK;
            mongoError = '';
        })
        .catch(mongoErr => {
            mongoStatus = STATUS_ERROR;
            mongoError = `(${mongoErr.message})`;
            hasError = true;
        })
        .then(() => {
            return pushApiChecker()
        })
        .then(() => {
            pushApiStatus = STATUS_OK;
            pushApiError = '';
        })
        .catch(pushErr => {
            pushApiStatus = STATUS_ERROR;
            pushApiError = `(${pushErr.message})`;
            hasError = true;
        })
        .then(() => {
            if (hasError) {
                isNot = ' NOT ';
            }
            let status = `${name} v${version}
work on nodejs ${process.version}
            
[ ${dbStatus} ] database "db" ${dbError}
[ ${mongoStatus} ] database "mongodb" ${mongoError}
[ ${pushApiStatus} ] service "push" ${pushApiError}

SERVICE${isNot}OPERATIONAL`;
            res.setHeader('Content-Type', 'text/plain');
            res.send(`${status}`);
        });
}

/**
 * Check mongo
 * @return {Promise}
 */
function mongoChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        mongoCheckInsert(key, 'push_test_set')
            .then(() => {
                return mongoCheckSelect(key)
            })
            .then(() => {
                return mongoCheckUpdate(key, 'push_test_update')
            })
            .then(() => {
                return mongoCheckDelete(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}


/**
 * Check db
 * @return {Promise}
 */
function dbChecker() {
    return new Promise((resolve, reject) => {
        let key = uuid();
        sqlManager.checkInsert(key, 'push_test_insert')
            .then(() => {
                return sqlManager.checkSelect(key)
            })
            .then(() => {
                return sqlManager.checkUpdate(key, 'push_test_update')
            })
            .then(() => {
                return sqlManager.checkDelete(key)
            })
            .then(() => {
                resolve('OK');
            })
            .catch(err => {
                reject(err);
            })
    })
}

function pushApiChecker() {
    return new Promise((resolve, reject) => {
        const requestOptions = {
            url: `${push_Api}version`
        };

        function sendRequest(error, response, body) {
            if (!error && parseInt(response.statusCode) === 200) {
                resolve();
            } else {
                if (error) {
                    reject(error);
                } else {
                    reject(new Error(`bad status code: ${response.statusCode}`))
                }
            }
        }

        request(requestOptions, sendRequest);
    })
}


module.exports = {
    version, status
};
