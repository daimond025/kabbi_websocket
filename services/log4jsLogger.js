'use strict';
const log4js = require('log4js');

log4js.configure({
    appenders: [
        {type: 'console'},
        {
            type: 'file',
            filename: 'logs/chat.log',
            category: 'chat',
            maxLogSize: 52428800,
            backups: 5

        },
        {
            type: 'file',
            filename: 'logs/order.log',
            category: 'order',
            maxLogSize: 52428800,
            backups: 5
        },
        {
            type: 'file',
            filename: 'logs/worker_socket.log',
            category: 'worker_socket',
            maxLogSize: 52428800,
            backups: 5

        }
    ]
});
const chatLog = log4js.getLogger('chat');
const orderLog = log4js.getLogger('order');
const workerSocketLog = log4js.getLogger('worker_socket');

/**
 * Chat logger
 * @param  {String} category
 * @param  {String} message
 */
function chatLogger(category, message) {
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message, null, 4);
    }
    switch (category) {
        case 'info':
            chatLog.info(message);
            break;
        case 'error':
            chatLog.error(message);
            break;
        default:
            chatLog.info(message);
    }
}


/**
 * Order logger
 * @param  {Number} orderId
 * @param  {String} category
 * @param  {String} message
 */
function orderLogger(orderId, category, message) {
    const prefix = orderId + '  ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message, null, 4);
    }
    message = prefix + message;
    switch (category) {
        case 'info':
            orderLog.info(message);
            break;
        case 'error':
            orderLog.error(message);
            break;
        default:
            orderLog.info(message);
    }
}


/**
 * Worker socket logger
 * @param  {String} workerQueueLabel
 * @param  {String} category
 * @param  {String} message
 */
function workerSocketLogger(workerQueueLabel, category, message) {
    const prefix = workerQueueLabel + '  ';
    if (typeof message === "object" && message !== null) {
        message = JSON.stringify(message, null, 4);
    }
    message = prefix + message;
    switch (category) {
        case 'info':
            workerSocketLog.info(message);
            break;
        case 'error':
            workerSocketLog.error(message);
            break;
        default:
            workerSocketLog.info(message);
    }
}

exports.chatLogger = chatLogger;
exports.orderLogger = orderLogger;
exports.workerSocketLogger = workerSocketLogger;

